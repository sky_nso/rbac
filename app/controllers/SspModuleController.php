<?php

class SspModuleController extends BaseController
{
    /**
     * Получение списка менеджеров.
     */
    const GET_MANAGER_URL = 'http://build.nromanov.ssp.smaclick.com/';

    /**
     * Получение списка платформ
     */
    const GET_SITE_URL = 'http://build.nromanov.ssp.smaclick.com/';

    const SITE_PART_STATISTIC = 'statistic/';

    /**
     * @var integer
     */
    public $user_id;

    /**
     * @var string
     */
    public $user_email;

    /**
     * @var array
     */
    public $countries_of_management;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $country_code;

    /**
     * @var array
     */
    public $ad_network;

    /**
     * @var integer
     */
    public $and_id;

    /**
     * @var string
     */
    public $adn_name;

    /**
     * @var string
     */
    public $adn_url;

    /**
     * @var integer
     */
    public $coordinator_id;

    /**
     * @var string
     */
    public $flo_webmaster;

    /**
     * @var integer
     */
    public $site_id;

    /**
     * @var string
     */
    public $site_title;

    /**
     * @var string
     */
    public $site_url;

    /**
     * @var integer
     */
    public $site_date;

    /**
     * @var integer
     */
    public $site_date_activate;

    /**
     * @var integer
     */
    public $site_date_del;

    /**
     * @var string
     */
    public $site_lang;

    /**
     * @var integer
     */
    public $adn_id;

    /**
     * @var integer
     */
    public $st_shows;

    /**
     * @var integer
     */
    public $st_clicks;

    /**
     * @var integer
     */
    public $dsp_leads;

    /**
     * @var integer
     */
    public $k_coefficient;

    /**
     * @var integer
     */
    public $income;

    /**
     * @var integer
     */
    public $outcome;

    /**
     * @var integer
     */
    public $yield;

    /**
     * @var boolean
     */
    public $code_availability;

    /**
     * @var integer
     */
    public $date_from;

    /**
     * @var integer
     */
    public $date_to;

    /**
     *
     * @return array
     */
    public function field_validation()
    {
        return [
            'user_id' => 'integer',
            'user_email' => 'string',
            'countries_of_management' => 'array',
            'id' => 'integer',
            'country_code' => 'integer',
            'ad_network' => 'array',
            'and_id' => 'integer',
            'adn_name' => 'string',
            'adn_url' => 'string',
            'coordinator_id' => 'integer',
            "site_id" => 'integer',
            "site_title" => "string",
            "site_url" => "string",
            "site_date" => 'integer',
            "site_date_activate" => 'integer',
            "site_date_del" => 'integer',
            "site_lang" => "string",
            "adn_id" => 'integer', // id рекламной сети
            "st_shows" => 'integer', // показы
            "st_clicks" => 'integer', // клики
            "dsp_leads" => "string", // лиды
            "k_coefficient" => "string", // К% площадки
            "income" => "float", // сумма доход площадки
            "outcome" => "float", // сумма расход площадки
            "yield" => 'integer', // сумма доходность площадки
            "code_availability" => "boolean", // установлен ли код
            "date_from" => 'integer', // dateFrom
            "date_to" => "int ", // dateTo
        ];
    }

    /**
     * Get full list ssp managers
     *
     * @return bool|array
     */
    public function getManagerList()
    {
        $this->setUrl(self::GET_MANAGER_URL.'?event=rest&a_event=getAllManagersInfo');

        return $this->getResult();
    }

    /**
     * Поиск по менеджерам
     *
     * @return array
     */
    public function findManager() : array
    {
        $managerList = $this->getManagerList();
        $result = [];
        ;
        foreach($managerList as $value){

            if(preg_match_all("/\b".$this->request->getQuery("manager_name", "string").'/', $value['user_email'])){
                $result[] = $value;
            }
        }

        return $result;
    }

    /**
     * @param null $id
     * @return bool|array
     */
    public function getManagerInfo($id = null)
    {
        $this->setUrl(self::GET_MANAGER_URL . $this->buildParamsUrl([$id]));

        return $this->getResult();
    }

    /**
     * Получаем список платформ
     *
     * @return bool|mixed
     */
    public function getSitesList()
    {
        $this->setUrl(self::GET_SITE_URL);

        return $this->getResult();
    }

    /**
     * Получаем информацию по одной платформа
     *
     * @param null $id
     * @return bool|mixed
     */
    public function getSitesInfo($id = null)
    {
        $this->setUrl(self::GET_SITE_URL . $this->buildParamsUrl([$id]));

        return $this->getResult();
    }

    /**
     * Получаем статистику по платформе
     *
     * @param null $id
     * @param null $date_from
     * @param null $date_to
     * @return bool|mixed
     */
    public function getSiteStatistic($id = null, $date_from = null, $date_to = null)
    {
        $this->setUrl(self::GET_SITE_URL . $this->buildParamsUrl([$id]) . self::SITE_PART_STATISTIC . $this->buildParamsUrl([$date_from, $date_to]));

        return $this->getResult();
    }

}