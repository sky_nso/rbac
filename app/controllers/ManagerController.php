<?php

use Phalcon\Mvc\Controller;

class ManagerController extends Controller
{
    public function getManagerInfo($id = [])
    {
        $manager = $this->getManager($id);

        return [
            'id' => $manager['id'],
            'name' => $manager['name'],
            'ad_network' => $this->getManagerAdNetwork($id),
            'geo' => $this->getManagerGeo($id),
            'supervisor' => $this->getManagerSupervision($id),
            'group' => $this->getManagerGroup($id),
        ];
    }

    public function getManagerGeo($id = 0)
    {
        return ['Serbia', 'Romania'];
    }

    public function getManagerSupervision($id = 0)
    {
        return [
            'id' => 1,
            'name' => 'Mike',
        ];
    }

    public function getManagerGroup($id = 0)
    {
        return [
            'id' => 55,
            'name' => 'sales-adnow'
        ];
    }

    public function getManagerSiteList($managerId = 0)
    {
        return range(1, rand(5, 20));
    }

    public function getManagerAdNetwork($id = 0)
    {
        return ['adnow', 'click'];
    }

    public function getManager($id = [])
    {
        $name = ['','Mike', 'Jefferson', 'Pablo', 'Ivan', 'Alfredo', 'George', 'Petr'];

        $index = rand(1,count($name)-1);
        return ['id' => $index, 'name' => $name[$index]];
    }
}