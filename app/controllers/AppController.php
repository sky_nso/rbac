<?php

class AppController extends BaseController
{
    /**
     * Возвращает список доступных экшенов
     *
     * @return mixed
     */
    public function indexAction()
    {
        $methodList = get_class_methods($this);

        $this->response->setJsonContent(
            [
                'data' => [
                    '/app/manager/common/',
                    '/app/manager/website/',
                    '/app/manager/history/',
                    '/app/manager/compare/',
                    '/app/manager/statistic/',
                    '/app/website/common/',
                    '/app/website/statistic/',
                    '/app/website/competitors/',
                    '/app/website/referrals/',
                    '/app/website/messages/',
                    '/app/website/test/',
                ]
            ]
        );

        return $this->response;
    }

    public function testAction()
    {
        $test['get'] = $_GET;
        $test['post'] = $this->request->getPost();
        $test['other'] = file_get_contents('php://input');
        print_r($test);
        die();
        $this->addResponse($test);
        return $this->response();
    }

    public function testtAction()
    {
        $data = ['name' => ['file' => '@/home/user/test.png']];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://192.168.11.152/app/website/test/');
        curl_setopt($ch, CURLOPT_HEADER, 'Content-Type: multipart/form-data');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        curl_exec($ch);
    }

    /**
     * Сводная личная статистика менеджера
     *
     * @return mixed
     */
    public function managerCommonAction()
    {
        $managerController = new ManagerController();
        $bitrixController = new BitrixController();

        $filter = Filter::get();

        $managerInfo = $managerController->getManagerInfo($filter['manager_ids']);

        $period = Filter::getPeriod();
        $item = 1;
        for ($i = 0; $i < $period['count_day']; $i++) {
            $bitrixInfo = $bitrixController->getCountSitesByStatus();

            $data[] = [
                'id' => $item++,
                'date_info' => [
                    'date' => $period['dates'][$i],
                ],
                'manager_info' => $managerInfo,
                'bitrix_sites' => $bitrixInfo,
                'ssp_sites' => [
                    'work' => [
                        'active' => rand(10, 100),
                    ],
                    'not_work' => [
                        'rejected' => rand(10, 100),
                        'deleted' => rand(10, 100),
                        'baned' => rand(10, 100),
                        'all' => 300
                    ],
                    'waiting' => [
                        'new' => rand(10, 100),
                    ],
                    'all' => 500,
                ],
                'messages' => [
                    'email' => rand(1000, 5000),
                    'phone' => [
                        'phones_incoming' => rand(10, 1000),
                        'phones_outgoing' => rand(10, 1000),
                        'phones_missed' => rand(10, 1000),
                        'all' => 3000
                    ],
                    'phone_balance' => [
                        'balance' => rand(10, 100),
                        'refill' => rand(10, 100),
                        'rate' => rand(10, 100),
                    ],
                    'all' => 8000
                ],
                'yield' => [
                    'coefficient' => rand(10, 20),
                    'rate' => rand(100, 500),
                    'profit' => rand(100, 300)
                ],
                'effectiveness' => [
                    'views' => rand(1000000000, 6000000000),
                    'clicks' => rand(10000, 60000),
                    'leads' => rand(100, 600),
                    'yield' => rand(100, 600),
                ],
                'site_information' => [
                    'site_type' => [
                        'spot' => 100,
                        'click' => 80,
                        'all' => 180
                    ],
                    'registration_method' => [
                        'ss' => 70,
                        'ma' => 20,
                        'all' => 90
                    ]
                ]
            ];
        }

        $summary = [];
        for ($i = 0; $i < count($data); $i++) {
            $summary['bitrix']['success'] += $data[$i]['bitrix_sites']['in_work']['success'];
            $summary['bitrix']['pause'] += $data[$i]['bitrix_sites']['not_work']['pause'];
            $summary['bitrix']['failed'] += $data[$i]['bitrix_sites']['not_work']['failed'];
            $summary['bitrix']['not_work']['all'] += $data[$i]['bitrix_sites']['not_work']['all'];
            $summary['bitrix']['initiated'] += $data[$i]['bitrix_sites']['waiting']['initiated'];
            $summary['bitrix']['collection_of_information'] += $data[$i]['bitrix_sites']['waiting']['collection_of_information'];
            $summary['bitrix']['offer'] += $data[$i]['bitrix_sites']['waiting']['offer'];
            $summary['bitrix']['negotiation_process'] += $data[$i]['bitrix_sites']['waiting']['negotiation_process'];
            $summary['bitrix']['waiting_for_reply'] += $data[$i]['bitrix_sites']['waiting']['waiting_for_reply'];
            $summary['bitrix']['confirmation'] += $data[$i]['bitrix_sites']['waiting']['confirmation'];
            $summary['bitrix']['payment'] += $data[$i]['bitrix_sites']['waiting']['payment'];
            $summary['bitrix']['placement_on_website'] += $data[$i]['bitrix_sites']['waiting']['placement_on_website'];
            $summary['bitrix']['waiting']['all'] += $data[$i]['bitrix_sites']['waiting']['all'];
            $summary['bitrix']['all'] += $data[$i]['bitrix_sites']['all'];

            $summary['ssp']['active'] += $data[$i]['ssp_sites']['work']['active'];
            $summary['ssp']['rejected'] += $data[$i]['ssp_sites']['not_work']['rejected'];
            $summary['ssp']['deleted'] += $data[$i]['ssp_sites']['not_work']['deleted'];
            $summary['ssp']['baned'] += $data[$i]['ssp_sites']['not_work']['baned'];
            $summary['ssp']['not_work']['all'] += $data[$i]['ssp_sites']['not_work']['all'];
            $summary['ssp']['new'] += $data[$i]['ssp_sites']['waiting']['new'];
            $summary['ssp']['all'] += $data[$i]['ssp_sites']['all'];

            $summary['messages']['email'] += $data[$i]['messages']['email'];
            $summary['messages']['phones_incoming'] += $data[$i]['messages']['phone']['phones_incoming'];
            $summary['messages']['phones_outgoing'] += $data[$i]['messages']['phone']['phones_outgoing'];
            $summary['messages']['phones_missed'] += $data[$i]['messages']['phone']['phones_missed'];
            $summary['messages']['phones']['all'] += $data[$i]['messages']['phone']['all'];
            $summary['messages']['balance'] += $data[$i]['messages']['phone_balance']['balance'];
            $summary['messages']['refill'] += $data[$i]['messages']['phone_balance']['refill'];
            $summary['messages']['rate'] += $data[$i]['messages']['phone_balance']['rate'];
            $summary['messages']['all'] += $data[$i]['messages']['all'];

            $summary['yield']['coefficient'] += $data[$i]['yield']['coefficient'];
            $summary['yield']['rate'] += $data[$i]['yield']['rate'];
            $summary['yield']['profit'] += $data[$i]['yield']['profit'];

            $summary['effectiveness']['views'] += $data[$i]['effectiveness']['views'];
            $summary['effectiveness']['clicks'] += $data[$i]['effectiveness']['clicks'];
            $summary['effectiveness']['leads'] += $data[$i]['effectiveness']['leads'];
            $summary['effectiveness']['yield'] += $data[$i]['effectiveness']['yield'];


            $summary['site_information']['spot'] += $data[$i]['site_information']['site_type']['spot'];
            $summary['site_information']['click'] += $data[$i]['site_information']['site_type']['click'];
            $summary['site_information']['site_type']['all'] += $data[$i]['site_information']['site_type']['all'];
            $summary['site_information']['ss'] += $data[$i]['site_information']['registration_method']['ss'];
            $summary['site_information']['ma'] += $data[$i]['site_information']['registration_method']['ma'];
            $summary['site_information']['registration_method']['all'] += $data[$i]['site_information']['registration_method']['all'];
        }

        $this->addResponse(['data' => $data, 'summary' => $summary, 'filter' => $period]);
        $this->successResponse();
        return $this->response();
    }

    /**
     * Возвращаем данные для таблицы статистики полной статискити
     *
     * @return mixed
     */
    public function managerWebsiteAction()
    {
        $clientController = new ClientController();
        $sspController = new SSPController();
        $simularController = new SimularWebController();
        $managerController = new ManagerController();

        $period = Filter::getPeriod();

        $data = [];
        $item = 1;
        //TODO добавить список нужных статустов (пока лежат внутри вызываемого метода)
        for ($i = 0; $i < $period['count_day']; $i++) {

            $adnow = rand(1, 10);
            $payclick = rand(1, 10);
            $adpop = rand(1, 10);
            $allOur += $adnow + $payclick + $adpop;

            $taboola = rand(1, 5);
            $ne_taboola = rand(1, 5);
            $allOther += $taboola + $ne_taboola;

            $all += $allOur + $allOther;

            $clientInfo = $clientController->getClientInfoById(1);
            $sspInfo = $sspController->getSiteInfoById(1);
            $similarWebInfo = $simularController->getSiteInfo('');

            $connectionStatus = $sspController->connectionInfo(1);
            $managerInfo = $managerController->getManagerInfo(1);

            $daysPassed = '';
            $historyLink = '';
            $managerComment = '';
            $buttonSendLink = '';

            $data[] = [
                'site_info' => ['date_connection' => $period['dates'][$i]],
                'client_info' => [
                    'site_name' => $clientInfo['site_name'],
                    'geo' => $clientInfo['geo'],
                    'owner_name' => $clientInfo['owner_name'],
                    'contacts' => $clientInfo['contacts'],
                    'site_type' => $clientInfo['site_type'],
                    'registration_type' => $clientInfo['registration_type'],
                ],
                'manager_ids1' => $_POST['manager_ids'],
                'ssp_info' => $sspInfo,
                'similarweb_info' => $similarWebInfo,
                'connection_status' => $connectionStatus,
                'manager_info' => $managerInfo,
                'last_message_date' => $clientInfo['last_message_date'],
                'days_passed' => $daysPassed,
                'history_link' => $historyLink,
                'manager_comment' => $managerComment,
                'button_send_comment_link' => 'parafail@yandex.ru',
                'client_statistic' => [
                    'coefficient' => $clientInfo['coefficient'],
                    'costs' => $clientInfo['costs'],
                    'yield' => $clientInfo['yield'],
                ],
                'client_appeal_statistic' => $clientInfo['appeal_statistic'],
                'site_information' => [
                    'all_blocks' => $all,
                    'our_blocks' => [
                        'adnow' => $adnow,
                        'payclick' => $payclick,
                        'adpop' => $adpop,
                        'all' => $allOur
                    ],
                    'competitor_blocks' => [
                        'taboola' => $taboola,
                        'ne_taboola' => $ne_taboola,
                        'all' => $allOther
                    ]
                ]
            ];
        }

        $summary = [];
        $summary['client']['registration_type']['ma'] = 1;
        $summary['client']['registration_type']['ss'] = 1;
        $summary['client']['site_type']['spot'] = 1;
        $summary['client']['site_type']['click'] = 1;
        for ($i = 0; $i < count($data); $i++) {

            if ($data[$i]['client_info']['registration_type'] == 'ma')
                $summary['client']['registration_type']['ma']++;
            if ($data[$i]['client_info']['registration_type'] == 'ss')
                $summary['client']['registration_type']['ss']++;
            if ($data[$i]['client_info']['site_type'] == 'spot')
                $summary['client']['site_type']['spot']++;
            if ($data[$i]['client_info']['site_type'] == 'click')
                $summary['client']['site_type']['click']++;

            $summary['client']['sites'] = count($data);
            $summary['ssp']['views'] += $data[$i]['ssp_info']['views'];
            $summary['ssp']['click'] += $data[$i]['ssp_info']['click'];
            $summary['ssp']['lead'] += $data[$i]['ssp_info']['lead'];

            $summary['similarweb']['views'] += $data[$i]['similarweb_info']['views'];
            $summary['similarweb']['click'] += $data[$i]['similarweb_info']['click'];
            $summary['similarweb']['lead'] += $data[$i]['similarweb_info']['lead'];

            $summary['client_statistic']['coefficient'] += $data[$i]['client_statistic']['coefficient'];
            $summary['client_statistic']['costs'] += $data[$i]['client_statistic']['costs'];
            $summary['client_statistic']['yield'] += $data[$i]['client_statistic']['yield'];

            $summary['client_appeal_statistic']['phone'] += $data[$i]['client_appeal_statistic']['phone'];
            $summary['client_appeal_statistic']['email'] += $data[$i]['client_appeal_statistic']['email'];
            $summary['client_appeal_statistic']['all'] += $data[$i]['client_appeal_statistic']['all'];

            $summary['message'] = count($data);

            $summary['site_information']['all_blocks'] += $data[$i]['site_information']['all_blocks'];
            $summary['site_information']['adnow'] += $data[$i]['site_information']['our_blocks']['adnow'];
            $summary['site_information']['payclick'] += $data[$i]['site_information']['our_blocks']['payclick'];
            $summary['site_information']['adpop'] += $data[$i]['site_information']['our_blocks']['adpop'];
            $summary['site_information']['our_blocks']['all'] += $data[$i]['site_information']['our_blocks']['all'];
            $summary['site_information']['taboola'] += $data[$i]['site_information']['competitor_blocks']['taboola'];
            $summary['site_information']['ne_taboola'] += $data[$i]['site_information']['competitor_blocks']['ne_taboola'];
            $summary['site_information']['competitor_blocks']['all'] += $data[$i]['site_information']['competitor_blocks']['all'];
        }

        $this->addResponse(['data' => $data, 'summary' => $summary]);
        $this->successResponse();
        return $this->response();

    }

    /**
     * История обращений менеджера
     *
     * @return array
     */
    public function managerHistoryAction()
    {
        $clientController = new ClientController();
        $sspController = new SSPController();
        $simularController = new SimularWebController();
        $managerController = new ManagerController();

        $period = Filter::getPeriod();
        $item = 1;
        for ($i = 0; $i < $period['count_day']; $i++) {
            $clientInfo = $clientController->getClientInfoById(1);
            $sspInfo = $sspController->getSiteInfoById(1);
            $similarWebInfo = $simularController->getSiteInfo('');
            $managerInfo = $managerController->getManagerInfo(1);

            //TODO раскидать статусы подключения по разным контроллерам
            $connectionStatus = $sspController->connectionInfo();

            //TODO надо брать из битрикаса
            $historyLink = '';
            $managerComment = '';
            $buttonSendLink = '';

            $data[] = [
                'id' => $item++,
                'message_id' => rand(1, 500),
                'last_time_connection' => $period['dates'][$i],
                'client_info' => [
                    'site_name' => $clientInfo['site_name'],
                    'id_client' => $clientInfo['id'],
                    'geo' => $clientInfo['geo'],
                    'owner_name' => $clientInfo['owner_name'],
                    'contacts' => $clientInfo['contacts'],
                    'site_type' => $clientInfo['site_type'],
                    'registration_type' => $clientInfo['registration_type'],
                    'ssp_connection_status' => $connectionStatus['ssp'],
                    'bitrix_connection_status' => $connectionStatus['bitrix24']
                ],
                'ssp_info' => array_merge($sspInfo, ['yield' => rand(500, 1000)]),
                'similarweb_info' => $similarWebInfo,
                'manager_info' => $managerInfo,
                'method_connection' => 'Телефон', //TODO выхватывать метод связи из битрикса
                'manager_comment' => $managerComment,
                'history_link' => $historyLink,
                'button_send_comment_link' => 'parafail@yandex.ru',
            ];
        }

        $this->addResponse(['data' => $data, 'summary' => $summary, 'filter' => $period]);
        $this->successResponse();
        return $this->response();

    }

    /**
     * Статистика по сайтам менеджеру
     *
     */
    public function managerWebsiteStatisticAction()
    {
        $clientController = new ClientController();
        $sspController = new SSPController();
        $simularController = new SimularWebController();
        $managerController = new ManagerController();

        $siteName = ['ya.ru', 'google.com', 'yahoo.com', 'groove.uk', 'pochta.ru'];

        $date_off = ['', strtotime('-' . rand(1, 100) . ' day')];

        $network = ['adnow', 'payclick', 'adpop'];

        $codeMark = ['yes', 'no'];

        $period = Filter::getPeriod();
        $item = 1;
        for ($i = 0; $i < $period['count_day']; $i++) {
            $dateLastMessage = strtotime('-' . rand(2, 10) . ' day');

            $datetime1 = new DateTime(date('Y-m-d', $dateLastMessage));
            $datetime2 = new DateTime(date('Y-m-d', strtotime('now')));
            $interval = $datetime1->diff($datetime2);
            $dayPassed = $interval->format('a');
            $clientInfo = $clientController->getClientInfoById(1);

            $data[] = [
                'id' => $item++,
                'date' => $period['dates'][$i],
                'date_connection' => strtotime(' - ' . $i . ' day'),
                'date_off' => $date_off[rand(0, 1)],
                'ad_network' => $network[rand(0, 2)],
                'code_mark' => $codeMark[rand(0, 1)],
                'client_info' => [
                    'site_name' => $clientInfo['site_name'],
                    'geo' => $clientInfo['geo'],
                    'owner_name' => $clientInfo['owner_name'],
                    'contacts' => $clientInfo['contacts'],
                    'site_type' => $clientInfo['site_type'],
                    'registration_type' => $clientInfo['registration_type'],
                ],
                'ssp_info' => $sspController->getSiteInfoById(1),
                'similar_info' => $simularController->getSiteInfo(''),
                'connection_status' => $sspController->connectionInfo(),
                'manager_info' => $managerController->getManagerInfo(1),
                'last_message_info' => [
                    'date_last_message' => $dateLastMessage,
                    'day_passed' => $dayPassed,
                ],
                'message_history' => 'ya.ru',
                'manager_comment' => '',
                'button_send_comment_link' => 'parafail@yandex.ru',
                'client_statistic' => [
                    'coefficient' => $clientInfo['coefficient'],
                    'costs' => $clientInfo['costs'],
                    'yield' => $clientInfo['yield'],
                ],
                'client_appeal_statistic' => $clientInfo['appeal_statistic'],
                'site_information' => [
                    'all_blocks' => 1000,
                    'our_blocks' => [
                        'adnow' => rand(1, 10),
                        'payclick' => rand(1, 10),
                        'adpop' => rand(1, 10),
                        'all' => 30
                    ],
                    'competitor_blocks' => [
                        'taboola' => rand(1, 5),
                        'ne_taboola' => rand(1, 5),
                        'all' => 10
                    ]
                ]
            ];
        }

        $views = 0;
        $graphic = [];
        $r = 4;
        for ($i = 0; $i < count($data); $i++) {
            $views = $data[$i]['effectiveness']['views'];
            $sni = rand(0, $r);
            $graphic[$i] = ['id' => $i];
            $graphic[$i]['site'] = $siteName[$sni];

            unset($siteName[$sni]);
            sort($siteName);
            $r--;
            for ($k = 0; $k < 7; $k++) {
                if ($k != 6)
                    $graphic[$i]['statistic'][$k]['value'] = rand(10000, $views);
                else
                    $graphic[$i]['statistic'][$k]['value'] = $views;
                $graphic[$i]['statistic'][$k]['date'] = date('Y-m-d', strtotime(' - ' . $k . ' day'));
                $views = $views - $graphic[$i]['statistic'][$k]['value'];
            }
        }

        $this->addResponse(['data' => $data, 'summary' => $summary, 'filter' => $period]);
        $this->successResponse();
        return $this->response();
    }

    /**
     * Статистика по менеджерам
     *
     * return array
     */
    public function managerStatisticAction()
    {
        $clientController = new ClientController();
        $sspController = new SSPController();
        $simularController = new SimularWebController();
        $managerController = new ManagerController();
        $bitrixController = new BitrixController();

        $period = Filter::getPeriod();
        $item = 1;
        for ($i = 0; $i < $period['count_day']; $i++) {
            $managerInfo = $managerController->getManagerInfo(1);

            $data[] = [
                'id' => $item++,
                'date' => $period['dates'][$i],
                'manager_info' => $managerInfo,
                'ssp_info' => $sspController->getSiteInfoById(1),
                'bitrix_info' => $bitrixController->getCountSitesByStatus(),
                'messages' => [
                    'email' => rand(1000, 5000),
                    'phone' => [
                        'phones_incoming' => rand(10, 1000),
                        'phones_outgoing' => rand(10, 1000),
                        'phones_missed' => rand(10, 1000),
                        'all' => 3000
                    ],
                    'phone_balance' => [
                        'balance' => rand(10, 100),
                        'refill' => rand(10, 100),
                        'rate' => rand(10, 100),
                    ],
                    'all' => 8000
                ],
                'yield' => [
                    'coefficient' => rand(10, 20),
                    'rate' => rand(100, 500),
                    'profit' => rand(100, 300)
                ],
                'effectiveness' => [
                    'views' => rand(1000000000, 6000000000),
                    'clicks' => rand(10000, 60000),
                    'leads' => rand(100, 600),
                    'yield' => rand(100, 600),
                ],
                'site_information' => [
                    'site_type' => [
                        'spot' => 100,
                        'click' => 80,
                        'all' => 180
                    ],
                    'registration_method' => [
                        'ss' => 70,
                        'ma' => 20,
                        'all' => 90
                    ]
                ]
            ];
        }
        $this->addResponse(['data' => $data, 'summary' => $summary, 'filter' => $period]);
        $this->successResponse();
        return $this->response();
    }

    /**
     * Статистика по сайтам
     *
     */
    public function websiteCommonAction()
    {
        $clientController = new ClientController();
        $sspController = new SSPController();
        $simularController = new SimularWebController();
        $managerController = new ManagerController();

        $dateData = strtotime('- ' . rand(1, 500) . ' day');
        $code = ['yes', 'no'];

        $managerInfo = $managerController->getManagerInfo(1);
        $clientInfo = $clientController->getClientInfoById(1);
        $sspInfo = $sspController->getSiteInfoById(1);
        $similarWebInfo = $simularController->getSiteInfo('');

        $allOur = 0;
        $all = 0;
        $allOther = 0;
        $period = Filter::getPeriod();
        $item = 1;
        for ($i = 0; $i < $period['count_day']; $i++) {
            $adnow = rand(1, 10);
            $payclick = rand(1, 10);
            $adpop = rand(1, 10);
            $allOur += $adnow + $payclick + $adpop;

            $taboola = rand(1, 5);
            $ne_taboola = rand(1, 5);
            $allOther += $taboola + $ne_taboola;

            $all += $allOur + $allOther;

            $data[] = [
                'id' => $item++,
                'date_data' => $period['dates'][$i],
                'date_connection' => strtotime(' - ' . $i . ' day'),
                'date_off' => strtotime(' - ' . $i . ' day'),
                'analysis_code' => $code[rand(0, 1)],
                'client_info' => [
                    'site_name' => $clientInfo['site_name'],
                    'geo' => $clientInfo['geo'],
                    'owner_id' => $clientInfo['id'],
                    'owner_name' => $clientInfo['owner_name'],
                    'contacts' => $clientInfo['contacts'],
                    'site_type' => $clientInfo['site_type'],
                    'registration_type' => $clientInfo['registration_type']
                ],
                'ssp_info' => array_merge($sspInfo, ['yield' => rand(500, 1000)]),
                'similarweb_info' => $similarWebInfo,
                'connection_status' => $sspController->connectionInfo(),
                'manager_info' => $managerInfo,
                'last_message_date' => $clientInfo['last_message_date'],
                'history_link' => 'ya.ru',
                'manager_comment' => '',
                'button_send_comment_link' => 'parafail@yandex.ru',
                'client_statistic' => [
                    'coefficient' => $clientInfo['coefficient'],
                    'costs' => $clientInfo['costs'],
                    'yield' => $clientInfo['yield'],
                ],
                'client_appeal_statistic' => $clientInfo['appeal_statistic'],
                'site_information' => [
                    'all_blocks' => $all,
                    'our_blocks' => [
                        'adnow' => $adnow,
                        'payclick' => $payclick,
                        'adpop' => $adpop,
                        'all' => $allOur
                    ],
                    'competitor_blocks' => [
                        'taboola' => $taboola,
                        'ne_taboola' => $ne_taboola,
                        'all' => $allOther
                    ]
                ]
            ];
        }

        for ($i = 0; $i < count($data); $i++) {

            if ($data[$i]['client_info']['registration_type'] == 'ma')
                $summary['client']['registration_type']['ma']++;
            if ($data[$i]['client_info']['registration_type'] == 'ss')
                $summary['client']['registration_type']['ss']++;
            if ($data[$i]['client_info']['site_type'] == 'spot')
                $summary['client']['site_type']['spot']++;
            if ($data[$i]['client_info']['site_type'] == 'click')
                $summary['client']['site_type']['click']++;

            $summary['client']['iteration'] = count($data);
            //$summary['ssp']['views'] += $data[$i]['ssp_info']['views'];
            $summary['ssp']['click'] += $data[$i]['ssp_info']['click'];
            $summary['ssp']['lead'] += $data[$i]['ssp_info']['lead'];

            $summary['ssp']['views']['all'] += $data[$i]['ssp_info']['views'];
            if (count($data) == 1) {
                $summary['ssp']['views']['last_day'] = $summary['ssp']['views']['all'];
            } else {
                $summary['ssp']['views']['last_day'] = $data[count($data) - 1]['ssp_info']['views'];
            }

            $summary['similarweb']['views'] += $data[$i]['similarweb_info']['views'];
            $summary['similarweb']['click'] += $data[$i]['similarweb_info']['click'];
            $summary['similarweb']['lead'] += $data[$i]['similarweb_info']['lead'];

            $summary['client_statistic']['coefficient'] += $data[$i]['client_statistic']['coefficient'];
            $summary['client_statistic']['costs'] += $data[$i]['client_statistic']['costs'];
            $summary['client_statistic']['yield'] += $data[$i]['client_statistic']['yield'];

            $summary['client_appeal_statistic']['phone'] += $data[$i]['client_appeal_statistic']['phone'];
            $summary['client_appeal_statistic']['email'] += $data[$i]['client_appeal_statistic']['email'];
            $summary['client_appeal_statistic']['all'] += $data[$i]['client_appeal_statistic']['all'];

            $summary['message'] = count($data);

            $summary['site_information']['all_blocks'] += $data[$i]['site_information']['all_blocks'];
            $summary['site_information']['adnow'] += $data[$i]['site_information']['our_blocks']['adnow'];
            $summary['site_information']['payclick'] += $data[$i]['site_information']['our_blocks']['payclick'];
            $summary['site_information']['adpop'] += $data[$i]['site_information']['our_blocks']['adpop'];
            $summary['site_information']['our_blocks']['all'] += $data[$i]['site_information']['our_blocks']['all'];
            $summary['site_information']['taboola'] += $data[$i]['site_information']['competitor_blocks']['taboola'];
            $summary['site_information']['ne_taboola'] += $data[$i]['site_information']['competitor_blocks']['ne_taboola'];
            $summary['site_information']['competitor_blocks']['all'] += $data[$i]['site_information']['competitor_blocks']['all'];
        }

        $this->addResponse(['data' => $data, 'summary' => $summary, 'filter' => $period]);
        $this->successResponse();
        return $this->response();
    }

    /**
     * История обращений к веб-мастеру сайта\платформы
     *
     */
    public function websiteHistoryMessageAction()
    {
        $clientController = new ClientController();
        $managerController = new ManagerController();
        $sspController = new SSPController();
        $simularController = new SimularWebController();

        $period = Filter::getPeriod();
        $item = 1;
        for ($i = 0; $i < $period['count_day']; $i++) {
            $connectionStatus = $sspController->connectionInfo();
            $clientInfo = $clientController->getClientInfoById(1);
            $data[] = [
                'id' => $item++,
                'id_message' => rand(1, 100),
                'date' => $period['dates'][$i],
                'client_info' => [
                    'site_name' => $clientInfo['site_name'],
                    'geo' => $clientInfo['geo'],
                    'owner_id' => $clientInfo['id'],
                    'owner_name' => $clientInfo['owner_name'],
                    'contacts' => $clientInfo['contacts'],
                    'site_type' => $clientInfo['site_type'],
                    'registration_type' => $clientInfo['registration_type'],
                    'ssp_connection_status' => $connectionStatus['ssp'],
                    'bitrix_connection_status' => $connectionStatus['bitrix24']
                ],
                'ssp_info' => $sspController->getSiteInfoById(1),
                'similar_info' => $simularController->getSiteInfo(''),
                'manager_info' => $managerController->getManagerInfo(1),
                'method_connection' => 'Телефон',
                'manager_comment' => '',
                'rejection_reason' => '', //Причина по который клиент отказался работать. TODO это поле убрать
                'history_message' => 'ya.ru',
                'button_send_comment_link' => 'parafail@yandex.ru',
            ];
        }

        $summary = [];
        for ($i = 0; $i < count($data); $i++) {
            $summary['message_count'] = count($data);

            $summary['ssp']['views'] += $data[$i]['ssp_info']['views'];
            $summary['ssp']['click'] += $data[$i]['ssp_info']['click'];
            $summary['ssp']['lead'] += $data[$i]['ssp_info']['lead'];
            $summary['ssp']['yield'] += $data[$i]['ssp_info']['yield'];

            $summary['similar']['views'] += $data[$i]['similar_info']['views'];
            $summary['similar']['click'] += $data[$i]['similar_info']['click'];
            $summary['similar']['lead'] += $data[$i]['similar_info']['lead'];

            $summary['manager_count'] = 1;


        }

        $this->addResponse(['data' => $data, 'summary' => $summary, 'filter' => $period]);
        $this->successResponse();
        return $this->response();
    }

    public function websiteStatisticNewSiteAction()
    {
        $geosite = ['RU','EN',"CN"];
        $simularController = new SimularWebController();
        $flag = ['no', 'yes'];
        $period = Filter::getPeriod();
        $item = 1;
        for ($i = 0; $i < $period['count_day']; $i++) {
            $data[] = [
                'id' => $item++,
                'date_data' => $period['dates'][$i],
                'date_appearance' => strtotime('-' . rand(1, 100) . ' day'),
                'flag' => $flag[rand(0, 1)],
                'site_name' => 'site#1',
                'similar_info' => $simularController->getSiteInfo(''),
                'blocs' => [
                    'all' => 10,
                    'competitors_blocks' => [
                        'all' => 10,
                        ['item_1' => 3, 'item_2' => 2, 'item_3' => 5]
                    ]
                ],
                'site_geo' => $geosite[rand(0,count($geosite)-1)],
            ];
        }

        $definition = ['item_1' => 'site1', 'item_2' => 'site2', 'item_3' => 'site3'];
        $this->addResponse(['data' => $data, 'summary' => $summary, 'filter' => $period, 'definition_competitors' => $definition]);
        $this->successResponse();
        return $this->response();

    }


    public function referralsAction()
    {
        $data = [];
        $period = Filter::getPeriod();
        $item = 1;
        for ($i = 0; $i < $period['count_day']; $i++) {
            $data[$i] = [
                'id' => $item++,
                'date' => $period['dates'][$i]
            ];

            for ($k = 1; $k <= 10; $k++ ){
                $data[$i]['referrals'][] = [
                    'id' => $k,
                    'name' => 'site'.$k,
                    'value' => rand(100000, 100000000000)
                ];
            }

            $data[$i]['other']['value'] =  rand(100000000000, 1000000000000);
        }

        for ($i = 0; $i < count($data); $i++) {

            for($k = 0; $k < count($data[$i]['referrals']); $k++){
                if($i > 0){
                    $summary[$data[$i]['referrals'][$k]['id']] = [
                        'id' => $data[$i]['referrals'][$k]['id'],
                        'value' => $data[$i]['referrals'][$k]['value'] + $data[$i-1]['referrals'][$k]['value']
                    ];
                } else {
                    $summary[$data[$i]['referrals'][$k]['id']] = [
                        'id' => $data[$i]['referrals'][$k]['id'],
                        'value' => $data[$i]['referrals'][$k]['value']
                    ];
                }
            }
            $summary['other'] += $data[$i]['other']['value'];
        }

        $this->addResponse(['data' => $data, 'filter' => $period, 'summary' => $summary]);
        $this->successResponse();
        return $this->response();

    }

    public function WebsiteMessagesAction()
    {
        $_POST['filter']['period']['period'] = 'week';
        $period = Filter::getPeriod();
        $item = 1;
        $manager = ['John Doe', 'Mark'];
        $owner = ['Alex', 'Jefferson'];
        $data = [];
        $summary = [];
        $author = ['manager', 'owner'];

        $manager = $manager[rand(0,1)];
        $owner = $owner[rand(0,1)];
        $id = rand(1,2);

        for ($i = 0; $i <  $period['count_day']; $i++) {
            $data[] = [
                'id' => $item++,
                'date' => $period['dates'][$i],
                'author' => $author[rand(0,1)],
                'message_id' => 1000+$item,
                'manager' => [
                    'id' => $id,
                    'name' => $manager,
                ],
                'owner' => [
                    'name' => $owner,
                ] ,
                'message' => 'Some message...',
                'platform' => 1,
            ];
        }


        $this->addResponse(['data' => $data, 'summary' => $summary, 'filter' => $period,]);
        $this->successResponse();
        return $this->response();
    }

    /**
     * Поиск манагера
     *
     * @param string $name
     * @return array
     */
    public function findManagerAction()
    {
        $name = $this->request->getPost('name');

        $sspModule = new SspModuleController();
        //$data = $sspModule->getManagerList();

        /*foreach ($data as $key=>$value){
            $data[$key]['flo_user'] = 'flo_name '.$key;
            $data[$key]['group_id'] = ''.$key;
            $data[$key]['group_name'] = 'group'.$key;
        }*/

        $data = [
            [
                "user_id" => "1",
                'flo_user' => 'Bar',
                "user_email"  => "teodorus@inbox.ru",
                'user_coordinator_id'  => "0",
                'adn_id'  => "17",
                'adn_name'  => "adnow",
                'adn_url'  => "adnow.com",
                "group_id" => '1',
                "group_name" => 'group1'
            ],
            [
                "user_id" => "2",
                'flo_user' => 'Foo',
                "user_email"  => "teodorus@inbox.ru",
                'user_coordinator_id'  => "1",
                'adn_id'  => "17",
                'adn_name'  => "adnow",
                'adn_url'  => "adnow.com",
                "group_id" => '2',
                "group_name" => 'group2'
            ],
            [
                "user_id" => "3",
                'flo_user' => 'Alfa',
                "user_email"  => "teodorus@inbox.ru",
                'user_coordinator_id'  => "1",
                'adn_id'  => "17",
                'adn_name'  => "adnow",
                'adn_url'  => "adnow.com",
                "group_id" => '3',
                "group_name" => 'group3'
            ],
            [
                "user_id" => "4",
                'flo_user' => 'Beta',
                "user_email"  => "teodorus@inbox.ru",
                'user_coordinator_id'  => "0",
                'adn_id'  => "17",
                'adn_name'  => "adclick",
                'adn_url'  => "adclick.com",
                "group_id" => '4',
                "group_name" => 'group4'
            ],
            [
                "user_id" => "5",
                'flo_user' => 'Delta',
                "user_email"  => "teodorus@inbox.ru",
                'user_coordinator_id'  => "0",
                'adn_id'  => "17",
                'adn_name'  => "adpop",
                'adn_url'  => "adpop.com",
                "group_id" => '5',
                "group_name" => 'group5'
            ],
        ];

        $filter = Filter::get();
        $coordinators = [];
        $result = [];
        if(!empty($filter['manager_name'])){
            foreach($data as $value){

                if(preg_match_all("/\b".$filter['manager_name'].'/', $value['flo_user'])){
                    $result[] = $value;

                    if($value['user_coordinator_id'] == 0){
                        $coordinators[] = $value;
                    }

                    $adnetworks[] = [
                        'adn_id'  => $value['adn_id'],
                        'adn_name'  => $value["adn_name"],
                        'adn_url'  => $value["adn_url"],
                    ];
                }
            }

            $data = $result;
        } else if(!empty($filter['manager_ids'])){
            foreach($data as $value){
                if( in_array($value['user_id'], $filter['manager_ids'])  ){
                    $result[] = $value;
                    if($value['user_coordinator_id'] == 0){
                        $coordinators[] = $value;
                    }

                    $adnetworks[] = [
                        'adn_id'  => $value['adn_id'],
                        'adn_name'  => $value["adn_name"],
                        'adn_url'  => $value["adn_url"],
                    ];
                }
            }

            $data = $result;
        } else {
            foreach ($data as $value){
                if($value['user_coordinator_id'] == 0){
                    $coordinators[] = $value;
                }

                $adnetworks[] = [
                    'adn_id'  => $value['adn_id'],
                    'adn_name'  => $value["adn_name"],
                    'adn_url'  => $value["adn_url"],
                ];
            }
        }

        $this->response->setJsonContent(
            [
                'data' =>
                    [
                        'managers' => $data,
                        'coordinators' => $coordinators,
                        'ad_networks' => $adnetworks

                    ]

            ]
        );
        return $this->response;
    }


}