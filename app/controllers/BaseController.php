<?php

use Phalcon\Mvc\Controller;

class BaseController extends Controller
{
    protected $curl;

    protected $curl_result;

    protected $curl_status_code = 0;

    protected $curl_url = '';

    public function setResponseMessage($message)
    {
        $this->addResponse(['message' => $message]);
    }

    public function successResponse()
    {
        $this->addResponse(
            [
                'status' => 'success'
            ]
        );

        $this->response->setStatusCode(200);
    }

    public function failResponse()
    {
        $this->addResponse(
            [
                'status' => 'fail'
            ]
        );

        $this->response->setStatusCode(400);
    }

    public function response()
    {
        return $this->response;
    }

    public function addResponse($additional = [])
    {
        if(json_decode($this->response->getContent(), true))
            $this->response->setJsonContent(array_merge(json_decode($this->response->getContent(), true),$additional));
        else
            $this->response->setJsonContent($additional);
    }

    /**
     * set property URL
     *
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->curl_url = $url;
    }

    /**
     * @return mixed
     */
    public function getCurlUrl() : string
    {
        return $this->curl_url;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->curl_status_code;
    }

    /**
     *  Создаем правильный URL с параметрами для GET запросов
     *
     * @param array $params
     * @return string
     */
    public function buildParamsUrl(array $params) : string
    {
        if (!is_array($params))
            return '';

        $params = array_filter(
            $params,
            function ($el) {
                return !empty($el);
            }
        );

        return implode('/', $params) . '/';
    }

    public function getResult()
    {
        $this->curl();

        if (empty($this->getCurlResult()))
            return false;

        $result = $this->toArray();

        $this->setProperty($result);

        return $result;
    }

    public function curl()
    {
        $this->curl = curl_init();

        curl_setopt($this->curl, CURLOPT_URL, $this->curl_url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);

        $this->curl_result = curl_exec($this->curl);
        $this->curl_status_code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        curl_close($this->curl);
    }

    /**
     * @return mixed
     */
    public function getCurlResult()
    {
        return $this->curl_result;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return json_decode($this->curl_result, true);
    }

    /**
     * Устанавливаем значения для свойств класса
     *
     * @param $params
     */
    public function setProperty($params = [])
    {
        if(empty($params))
            return;

        foreach ($params as $key => $value) {
            if (property_exists($this, $key))
                $this->$key = $value;
        }
    }
}