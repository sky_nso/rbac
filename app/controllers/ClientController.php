<?php

use Phalcon\Mvc\Controller;

class ClientController extends Controller
{
    /**
     * @param int $id
     * @return array
     */
    public function getClientInfoById($id = 0)
    {
        $siteType = ['spot', 'click'];
        $registrationType = ['ma', 'ss'];

        return [
            'site_name' => 'test1',
            'geo' => 'eu',
            'id' => rand(1,10),
            'owner_name' => 'Alfredo Lemos',
            'contacts' => [
                'email' => 'alemos@mail.com',
                'skype' => 'lemos_alf',
                'other' => ''
            ],
            'site_type' => $siteType[rand(0, 1)],
            'registration_type' => $registrationType[rand(0, 1)],
            'coefficient' => 63,
            'costs' => 6,
            'yield' => 15,
            'appeal_statistic' => [
                'all' => 3,
                'email' => 1,
                'phone' => 2,
            ]
        ];
    }
}