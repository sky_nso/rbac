<?php

use Phalcon\Mvc\Controller;

class SSPController extends Controller
{

    /**
     * @param int $clientId
     */
    public function getClientSiteList($clientId = 0)
    {
        $sspObj = new SSP();

        return $sspObj->getSiteList();
    }

    /**
     * @param int $siteId
     * @return array
     */
    public function getSiteInfoById($siteId = 0)
    {
        return [
            'views' => rand(100, 5000000),
            'click' => rand(100, 5000000),
            'lead' => rand(100, 50000000),
            'yield' => rand(100, 50000000),
        ];
    }

    /**
     * @param int $siteId
     * @return array
     */
    public function connectionInfo($siteId = 0)
    {
        $status = ['Подключен', 'Регистрируется', 'Заключаем договор'];
        return [
            'ssp' => $status[rand(0,2)],
            'bitrix24' => $status[rand(0,2)],
        ];
    }

    public function setFilter($params = [])
    {

    }

    public function getFilter()
    {

    }

}