<?php

use Phalcon\Mvc\Controller;

class BitrixController extends Controller
{

    public function getCountSitesByStatus($status = [])
    {
        //TODO вынести все возможные статусы в другое место
        if (empty($status)) {
            $status = ['in_work', 'waiting'];
        }

        $siteCount = [];
        $siteCount['all'] = 0;

        foreach ($status as $value) {
            //Формируем фильтер
            $filter = ['status' => $value];
            //Получаем данные по сайтам
            $siteInfo = $this->getSite($filter);

            $siteCount[$value]['all'] = 0;
            //Перебирем сайты и считаем кол-во
            foreach ($siteInfo as $info){
                $siteCount[$value][$info] = count($info);
                $siteCount[$value]['all'] += $siteCount[$value][$info];
            }

            //Считаем общее количество сайтов
            $siteCount['all'] += $siteCount[$value]['all'];
        }

        return [
            'in_work' => [
                'success' => rand(10, 100),
            ],
            'not_work' => [
                'pause' => rand(10, 100),
                'failed' => rand(10, 100),
                'all' => 200
            ],
            'waiting' => [
                'initiated' => rand(10, 100),
                'collection_of_information' => rand(10, 100),
                'offer' => rand(10, 100),
                'negotiation_process' => rand(10, 100),
                'waiting_for_reply' => rand(10, 100),
                'confirmation' => rand(10, 100),
                'payment' => rand(10, 100),
                'placement_on_website' => rand(10, 100),
                'all' => 800,
            ],
            'all' => 1000
        ];
    }

    /**
     * По фильтру отбираем нужные сайты
     *
     * @param array $filter
     * @return array
     */
    public function getSite($filter = [])
    {
        return [];
    }


    public function getManagerList()
    {

    }

    public function getFilter()
    {

    }

    public function setFilter()
    {

    }
}