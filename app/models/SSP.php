<?php

class SSP extends BaseModel
{
    const SSP_URL = '';

    public $method = '';

    public $params = [];

    /**
     * Получаем список сайтов менеджера
     *
     * @param int $managerID
     * @return array
     */
    public function getSiteList($managerID = 0)
    {
        //Устанавливаем метод
        $this->setMethod('');
        //Устанавлием параметр
        $this->setFilter(['idmanager' => $managerID]);

        //TODO вот от этого надо бует избавиться когда будут реальные данные
        $result = [];
        for ($i = 0; $i < rand(5, 15); $i++){
            $result[] = $this->getSiteById();
        }

        return $result;
        //отдаем результат
        return $this->requestSSP();





        $siteType = ['spot', 'click'];
        $registrationType = ['MA', 'SS'];
        return [
            "data" => [
                [
                    'client_info' => [
                        'site_name' => 'test1',
                        'geo' => 'eu',
                        'owner_name' => 'Alfredo Lemos',
                        'contacts' => [
                            'email' => 'alemos@mail.com',
                            'skype' => 'lemos_alf',
                            'other' => ''
                        ],
                        'site_type' => $siteType[rand(0, 1)],
                        'registration_type' => $registrationType[rand(0, 1)],
                    ],
                    'ssp_info' => [
                        'views' => rand(100, 5000000),
                        'click' => rand(100, 5000000),
                        'lead' => rand(100, 50000000),
                    ],
                    'similarweb_info' => [
                        'views' => rand(100, 5000000),
                        'click' => rand(100, 5000000),
                        'lead' => rand(100, 50000000),
                    ],
                    'connection_status' => [
                        'ssp' => '',
                        'bitrix24' => ''
                    ],
                    'manager_info' => [
                        'manager' => [
                            'id' => 2,
                            'name' => 'Jefferson',
                        ],
                        'supervisor' => [
                            'id' => 1,
                            'name' => 'Mike',
                        ],
                        'group' => [
                            'id' => 55,
                            'name' => 'sales-adnow'
                        ],
                    ],
                    'last_massage_date' => date('d.m.Y'),
                    'days_passed' => 1,
                    'history_link' => 'ya.ru',
                    'manager_comment' => '',
                    'button_send_comment_link' => 'ya.ru?id=2',
                    'client_statistic' => [
                        'coefficient' => 63,
                        'costs' => 6,
                        'yield' => 15,
                    ],
                    'client_appeal_statistic' => [
                        'all' => 3,
                        'email' => 1,
                        'phone' => 2,
                    ]
                ]
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getManagerList()
    {
        //Прописываем метод для получение списка пользователей
        $this->setMethod('');

        return $this->requestSSP();
    }

    /**
     * @param int $platformId
     * @return mixed
     */
    public function getSiteById($platformId = 0)
    {
        $this->setMethod('');
        $this->setFilter(['idplatform' => $platformId]);

        $siteType = ['spot', 'click'];
        $registrationType = ['MA', 'SS'];

        return [
            'idplatform' => rand(1,1000),
            'nameplatform' => 'site_name'.rand(1,1000),
            'flowebplatform' => 'Alfredo Lemos',
            'statusSSP' => 'active',
            'typeplatform' => $siteType[rand(0, 1)],
            'dataregister' => strtotime(date('now')),
            'methodregister' => $registrationType[rand(0, 1)],
            'impressionSSP' => rand(100, 5000000),
            'clickSSP' =>  rand(100, 5000000),
            'lidesSSP' => rand(100, 5000000),
            'KprocSSP' => rand(100, 5000),
            'incomeSSP' => rand(10, 100),
            'outcomeSSP' => rand(10, 100),
        ];



        return $this->requestSSP();
    }

    /**
     * @return mixed
     */
    public function requestSSP()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::SSP_URL.$this->getMethod());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getFilter());

        $result = curl_exec($ch);

        return json_decode($result, true);
    }

    /**
     * @param array $params
     */
    public function setFilter($params = [])
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getFilter()
    {
        return $this->params;
    }

    /**
     * @param string $method
     */
    public function setMethod($method = ''){
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }
}

