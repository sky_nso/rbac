<?php

class Formatter extends BaseModel
{
    const TIME_ZONE = 'Europe/Moscow';

    public static function DateTime($date,$timezone = '')
    {
        $timezone == '' ? self::TIME_ZONE : $timezone;

        $d = date_create($date.' '.$timezone);
        return $d->getTimestamp();
    }


}