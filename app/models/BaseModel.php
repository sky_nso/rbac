<?php

class BaseModel extends Phalcon\Mvc\Model
{
    protected $curl;

    protected $curl_result;

    protected $curl_status_code = 0;

    protected $curl_url = '';

    /**
     * set property URL
     *
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->curl_url = $url;
    }

    /**
     * @return mixed
     */
    public function getCurlUrl() : string
    {
        return $this->curl_url;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->curl_status_code;
    }

    /**
     *  Создаем правильный URL с параметрами для GET запросов
     *
     * @param array $params
     * @return string
     */
    public function buildParamsUrl(array $params) : string
    {
        if (!is_array($params))
            return '';

        $params = array_filter(
            $params,
            function ($el) {
                return !empty($el);
            }
        );

        return implode('/', $params) . '/';
    }

    public function getResult()
    {
        $this->curl();

        if (empty($this->getCurlResult()))
            return false;

        $result = $this->toArray();

        $this->setProperty($result);

        return $result;
    }

    public function curl()
    {
        $this->curl = curl_init();

        curl_setopt($this->curl, CURLOPT_URL, $this->curl_url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);

        $this->curl_result = curl_exec($this->curl);
        $this->curl_status_code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        curl_close($this->curl);
    }

    /**
     * @return mixed
     */
    public function getCurlResult()
    {
        return $this->curl_result;
    }

    /**
     * @return mixed
     */
    public function toArray()
    {
        return json_decode($this->curl_result, true);
    }

    /**
     * Устанавливаем значения для свойств класса
     *
     * @param $params
     */
    public function setProperty($params = [])
    {
        if(empty($params))
            return;

        foreach ($params as $key => $value) {
            if (property_exists($this, $key))
                $this->$key = $value;
        }
    }

}