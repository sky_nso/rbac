<?php

class Filter
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    public static function getPeriod()
    {
        $filter = self::get();

        $dateFrom = strtotime(date('Y-m-d 00:00:00'));
        $dateTo = strtotime('now');
        $period = 'today';
        $countDay = 1;
        $dates[] = $dateTo;

        if (isset($filter['period'])) {

            if (isset($filter['period']['period'])) {
                switch ($filter['period']['period']) {
                    case 'today':
                        break;
                    case 'yesterday':
                        $dateFrom = strtotime(date('Y-m-d 00:00:00',strtotime('-1 day')));
                        $dateTo = strtotime(date('Y-m-d 23:59:59',strtotime('-1 day')));
                        $period = 'yesterday';
                        break;
                    case 'week';
                        $dateFrom = strtotime('-1 week');
                        $dateTo = strtotime(date('Y-m-d 00:00:00',strtotime('-1 day')));
                        $period = $filter['period']['period'];
                        break;
                    case 'month':
                        $dateFrom = strtotime('-1 month');
                        $dateTo = strtotime('now');
                        $period = $filter['period']['period'];
                        break;
                }
            }

            if (isset($filter['period']['from']) && is_numeric($filter['period']['from'])) {
                $dateFrom = $filter['period']['from'];
                $period = 'Период';
            }

            if (isset($filter['period']['to']) && is_numeric($filter['period']['to'])) {
                $dateTo = $filter['period']['to'];
                $period = 'Период';
            }

            $from = new DateTime(date('Y-m-d', $dateFrom));
            $to = new DateTime(date('Y-m-d 23:59:59', $dateTo));

            $datePeriod = new DatePeriod($from, new DateInterval('P1D'), $to);

            $dates = array_map(
                function ($item) {
                    return strtotime($item->format(self::DATE_FORMAT));
                },
                iterator_to_array($datePeriod)
            );

            $countDay = count($dates);//($dateTo - $dateFrom) / (60 * 60 * 24);
        }

        return ['period' => $period, 'date_from' => $dateFrom, 'date_to' => $dateTo, 'count_day' => $countDay, 'dates' => $dates];
    }

    public static function get()
    {
        $request = new Phalcon\Http\Request;

        $filter = [];
        $input = file_get_contents('php://input');

        if (!empty($input)) {
            $filter = json_decode($input, true);
            $filter = $filter['filter'];
        }

        if (!empty($request->getPost('filter'))) {
            $filter = array_merge($filter, $request->getPost('filter'));
        }

        if(!empty($_GET['filter'])){
            $filter = array_merge($filter, $_GET['filter']);
        }

        return $filter;
    }
}