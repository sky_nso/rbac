<?php

use Phalcon\Loader;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\ModuleDefinitionInterface;

$config = new Phalcon\Config\Adapter\Ini("../app/config/config.ini", INI_SCANNER_NORMAL);

// автозагрузчик
$loader = new Loader();

// Регистрируем классы
$loader->registerDirs(
    [
        $config->phalcon->controllersDir,
        $config->phalcon->modelsDir,
        $config->phalcon->modulesDir
    ], true
);

$loader->register();

// Создаем DI
$di = new FactoryDefault();

//Connect DB
$di->set(
    "db",
    function () {
        return new DbAdapter(
            [
                "host"     => "127.0.0.1",
                "username" => "root",
                "password" => "",
                "dbname"   => "rs",
            ]
        );
    }
);

// Настраиваем компонент View
$di->set(
    "view",
    function () {
        $view = new View();

        $view->setViewsDir("../app/views/");
        return $view;
    }
);

// Настраиваем базовый URI так, чтобы все генерируемые URI содержали корневую директорию "/"
$di->set(
    "url",
    function () {
        $url = new UrlProvider();

        $url->setBaseUri("/");
        return $url;
    }
);

//Добавляем зависимость дял запросов
$di->set(
    "request",
    new Request()
);

$di->set(
    "router",
    function () {
        $router = new Router();

        $router->setUriSource("../app/controllers");

        //Статистика по сайтам
        $router->add(
            "/app/manager/common/",
            [
                "controller" => "app",
                "action"     => "managerCommon",
            ]
        );

        //Сводная личная статистика
        $router->add(
            "/app/manager/website/",
            [
                "controller" => "app",
                "action"     => "managerWebsite",
            ]
        );

        //История обращений
        $router->add(
            "/app/manager/history/",
            [
                "controller" => "app",
                "action"     => "managerHistory",
            ]
        );

        //Сводная статситика по сайту
        $router->add(
            "/app/manager/compare/",
            [
                "controller" => "app",
                "action"     => "managerWebsiteStatistic",
            ]
        );

        //Статистика по менеджерам
        $router->add(
            "/app/manager/statistic/",
            [
                "controller" => "app",
                "action"     => "managerStatistic",
            ]
        );

        $router->add(
            "/app/manager/comparison/",
            [
                "controller" => "app",
                "action"     => "managerStatistic",
            ]
        );

        $router->add(
            "/app/website/common/",
            [
                "controller" => "app",
                "action"     => "websiteCommon",
            ]
        );

        $router->add(
            "/app/website/statistic/",
            [
                "controller" => "app",
                "action"     => "websiteHistoryMessage",
            ]
        );

        $router->add(
            "/app/website/competitors/",
            [
                "controller" => "app",
                "action"     => "websiteStatisticNewSite",
            ]
        );

        $router->add(
            "/app/website/test/",
            [
                "controller" => "app",
                "action"     => "test",
            ]
        );

        $router->add(
            "/app/website/referrals/",
            [
                "controller" => "app",
                "action"     => "referrals",
            ]
        );

        $router->add(
            "/app/website/messages/",
            [
                "controller" => "app",
                "action"     => "WebsiteMessages",
            ]
        );

        $router->add(
            "/app/find/manager/",
            [
                "controller" => "app",
                "action"     => "findManager",
            ]
        );



        return $router;
    }
);

$application = new Application();

$application->setDI($di);

try {
    // Обрабатываем запрос
    $response = $application->handle();

    $response->send();
} catch (\Exception $e) {

    $response = new Response();
    $response->setJsonContent(
        [
            "message" => "service is failed",
            "service" => $e->getMessage()
        ]
    );

    $response->setStatusCode(400);
    $response->send();
}
